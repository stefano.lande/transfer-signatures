import org.bouncycastle.asn1.{ASN1EncodableVector, ASN1Integer, DERSequence}

import java.math.BigInteger
import java.security.spec.X509EncodedKeySpec
import java.security.{KeyFactory, Signature}
import java.util
import java.util.Base64

object SignatureVerification extends App {

  val signature = Signature.getInstance("SHA256withECDSA")
  val pubKeySpec= new X509EncodedKeySpec(Base64.getDecoder.decode("MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEVM3pAAn/3X62axYB1blE0AjxGZ+u9mf01qG2B9F6xX7fREDswWvMglYmotJjoH3JVvzhEXsXQAdAr+PqwGxbAw=="))
  val keyFactory =  KeyFactory.getInstance("EC")
  val pubKey = keyFactory.generatePublic(pubKeySpec)

  val message = "POSTE ITALIANE SPA COO-SBO-OP GEST. PATRIMONIALE MOBILIARE-RIMBORSIIT21J0760103200001046440267MF-WRP-006791.272021-06-23false"
  val signatureString = "Y2Qas4T3m3JbzfEEv0eAJwW7Xbne8RtuMI2AQiMUbw/r4QyvZNus7lMVS6hLqeENasq5ynRuipJI1tNaNjNVMQ=="

  val jsSig = plainToDer(Base64.getDecoder.decode(signatureString))

  signature.initVerify(pubKey)
  signature.update(message.getBytes)
  val res = signature.verify(jsSig)

  println(s"Verification result $res")

  def plainToDer(plain: Array[Byte]): Array[Byte] = {
    val n = 32

    val r = new BigInteger(+1, util.Arrays.copyOfRange(plain, 0, n))
    val s = new BigInteger(+1, util.Arrays.copyOfRange(plain, n, n * 2))


    val v = new ASN1EncodableVector
    v.add(new ASN1Integer(r))
    v.add(new ASN1Integer(s))
    new DERSequence(v).getEncoded
  }
}
