package model

import java.time.LocalDate
import spray.json.{JsBoolean, JsObject, JsValue, enrichAny}
import model.TransferProtocol._

case class PreTransfer(creditorName: String,
                       creditorIban: String,
                       reference: String,
                       amount: BigDecimal,
                       executionDate: LocalDate,
                       isUrgent: Boolean,
                       userId: Option[Long] = None,
                       signature: Option[String] = None) {

  def toMessage: String = {

    val creditor = this.creditorName.toUpperCase
    val iban = this.creditorIban
    val description = this.reference.toUpperCase
    val amount = this.amount.toString
    val date = this.executionDate.toString

    creditor + iban + description + amount + date
  }

  def toQueueMessage(userId: Long, signature: String): JsValue =
    JsObject(this.copy(userId = Some(userId), signature = Some(signature)).toJson.asJsObject.fields + ("isUrgent" -> JsBoolean(false)))
}
