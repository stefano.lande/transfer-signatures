package model

import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, deserializationError}

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import scala.util.Try

object TransferProtocol extends DefaultJsonProtocol {
  implicit val localDateFormat = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue = JsString(formatter.format(obj))

    override def read(json: JsValue): LocalDate = {
      json match {
        case JsString(lDString) =>
          Try(LocalDate.parse(lDString, formatter)).getOrElse(deserializationError(deserializationErrorMessage))
        case _ => deserializationError(deserializationErrorMessage)
      }
    }

    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE
    private val deserializationErrorMessage =
      s"Expected date time in ISO offset date time format ex. ${LocalDate.now().format(formatter)}"
  }


  implicit val preTransferFormat = jsonFormat8(PreTransfer)
}