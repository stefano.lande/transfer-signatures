import model.PreTransfer
import org.bouncycastle.asn1.{ASN1Integer, ASN1Sequence}

import java.math.BigInteger
import java.security.spec.PKCS8EncodedKeySpec
import java.security.{KeyFactory, Signature}
import java.time.LocalDate
import java.util.{Base64, UUID}

object ComputeSignature extends App {

  val signature = Signature.getInstance("SHA256withECDSA")

  val privKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder.decode("MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgPFynh2BqGcxaXuasyRLGShGBGRrmHcxq1EPFO74+c8yhRANCAASUBrmRqXncHSIkGc878duAci7WllVtcyONWj7Px5oYHmeGYhFf7t3uobzB6oLWmpdMzhbkIQR4mJlGn2hLkUjX"))

  val keyFactory =  KeyFactory.getInstance("EC")

  val privKey = keyFactory.generatePrivate(privKeySpec)
  
  val preTransfer = PreTransfer(
    creditorName = "Ulrich Nielsen",
    creditorIban = "DE70500105177364247665",
    reference = "WITHDRAWAL TEST"  + UUID.randomUUID(),
    amount = BigDecimal(20.00),
    isUrgent = false,
    executionDate = LocalDate.now())


  val message = preTransfer.toMessage


  signature.initSign(privKey)
  signature.update(message.getBytes)

  val sig = Base64.getEncoder.encodeToString(derToConcatenated(signature.sign()))
  val userId = 6999

  println(s"Signed message: $message")
  println(s"Signature: $sig")
  println(s"Queue message\n${preTransfer.toQueueMessage(6999, sig)}")

  def derToConcatenated(der: Array[Byte]): Array[Byte] = {
    val n = 32 // assume 256-bit-order curve like P-256

    val seq = ASN1Sequence.getInstance(der)
    val r = seq.getObjectAt(0).asInstanceOf[ASN1Integer].getValue
    val s = seq.getObjectAt(1).asInstanceOf[ASN1Integer].getValue

    val out = new Array[Byte](2 * n)
    toFixed(r, out, 0, n)
    toFixed(s, out, n, n)
    out
  }

  private def toFixed(x: BigInteger, a: Array[Byte], off: Int, len: Int): Unit = {
    val t = x.toByteArray
    if (t.length == len + 1 && t(0) == 0) System.arraycopy(t, 1, a, off, len)
    else if (t.length <= len) System.arraycopy(t, 0, a, off + len - t.length, t.length)
    else throw new Exception
  }
}
