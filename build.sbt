name := "stash"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies += "org.bouncycastle" % "bcprov-jdk15on" % "1.68"
libraryDependencies += "io.spray" %%  "spray-json" % "1.3.6"
